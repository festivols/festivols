﻿# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Hôte: 127.0.0.1 (MySQL 5.7.31)
# Base de données: festivols
# Temps de génération: 2020-09-02 21:52:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occurence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `name`, `description`, `occurence`)
VALUES
	(1,'Animation Babord','Lorem ipsum','14h-20h'),
	(2,'Animation Tribord','Lorem ipsum','14h-20h'),
	(3,'Bar Havana','Lorem ipsum','15h-19h'),
	(4,'Bar Coca','Lorem ipsum','15h-19h'),
	(5,'Stand Entrée Après-Midi','Lorem ipsum','13h-18h'),
	(6,'Stand Entrée Soir','Lorem ipsum','17h-22'),
	(7,'Stand Tribord Après-Midi','Lorem ipsum','19h-22H');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `label`)
VALUES
	(1,'admin','organisateur'),
	(2,'volunteer','bénévole'),
	(3,'blacklisted','Blacklisté'),
	(4,'team-1','Équipe « Animation »'),
	(5,'team-2','Équipe « Bar »'),
	(6,'team-3','Équipe « Cashless »');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table shifts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shifts`;

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `nb_volunteers` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `shifts` WRITE;
/*!40000 ALTER TABLE `shifts` DISABLE KEYS */;

INSERT INTO `shifts` (`id`, `post_id`, `team_id`, `nb_volunteers`)
VALUES
	(9,1,1,10),
	(10,2,1,10),
	(11,3,2,15),
	(12,4,2,20),
	(13,5,3,10),
	(14,6,3,2),
	(15,7,3,5);

/*!40000 ALTER TABLE `shifts` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nb_max` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `name`, `nb_max`)
VALUES
	(1,'Animation','15'),
	(2,'Bar','34'),
	(3,'Cashless','10');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email_confirmed_at` datetime DEFAULT NULL,
  `is_blacklisted` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `birthday` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `email_confirmed_at`, `is_blacklisted`, `password`, `active`, `first_name`, `last_name`, `address`, `phone`, `birthday`)
VALUES
	(1,'admin@d-delfosse.be','2020-09-02 16:28:08',0,'$2b$12$s0FuK8cws7uqmIPo1n8Bju.2pT3VSRdYgIigHZ5FOSpeV0ltwNZZy',1,'Danielle','Delfosse','Rue Jean Mottin 18, 4280 Hannut','+32 498 63 09 11','21/09/1995'),
	(2,'fowlartemis@protonmail.ch','2020-09-02 16:28:39',0,'$2b$12$LmJYAQRonJZ6zBFrH80Qv.bCnbcaxvPKCFXKeA1B.oqosG5MY/1Eu',1,'Artemis','Fowl','Av. du Paradis 13, 1490 Court Saint Etienne','+32 456 55 56 97','1996-05-12'),
	(3,'9s_hacker02w@civoo.com','2020-09-02 16:32:42',0,'$2b$12$vkGFsKVGk8uci5KpAUa5ruQFCqjICOEansdY.ORD9hWYfqpGzor2y',1,'Dela Robia','Odd','Av. de l\'EPHEC','+32 456 55 53 39','1994-07-12'),
	(4,'jnice.hmwdyo@dpics.fun','2020-09-02 16:34:07',0,'$2b$12$4IfKSnH8MWKO.P/9zkiBzujL./FrcD5tpV6bYtK0uikhMyyxVx8TG',1,'Belpois','Jeremy','Av. de l\'EPHEC','+32 456 55 52 83','1994-07-12'),
	(5,'9rickcollingwo@timesua.com','2020-09-02 16:36:04',1,'$2b$12$U/3wGS8pnELtsDDe8GpEveFBdMhRR31xE3SKszFCi1r1kGD9eeyVS',1,'Delartre','Ulrich','Av. de l\'EPHEC','+32 495 55 36 64','1994-07-12');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table users_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_roles`;

CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;

INSERT INTO `users_roles` (`id`, `user_id`, `role_id`)
VALUES
	(1,1,1),
	(2,2,2),
	(3,3,2),
	(6,5,3),
	(7,3,4),
	(8,1,5),
	(9,2,6),
	(10,4,2),
	(11,4,5);

/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table users_team
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_team`;

CREATE TABLE `users_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_teamleader` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `users_team_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_team_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users_team` WRITE;
/*!40000 ALTER TABLE `users_team` DISABLE KEYS */;

INSERT INTO `users_team` (`id`, `team_id`, `user_id`, `is_teamleader`)
VALUES
	(1,1,3,1),
	(2,2,1,1),
	(3,3,2,1);

/*!40000 ALTER TABLE `users_team` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
